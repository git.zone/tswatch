import * as early from '@pushrocks/early';
early.start('tswatch');
export * from './tswatch.classes.tswatch.js';
export * from './tswatch.cli.js';
early.stop();
