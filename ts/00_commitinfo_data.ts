/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@gitzone/tswatch',
  version: '2.0.7',
  description: 'watch typescript projects during development'
}
